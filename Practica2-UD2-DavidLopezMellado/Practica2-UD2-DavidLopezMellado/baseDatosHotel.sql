CREATE DATABASE if not exists datoshotel;
--
USE datoshotel;
--
create table if not exists clientes(
idcliente int auto_increment primary key,
nombre varchar(50) not null,
apellidos varchar(50) not null,
fechanacimiento date,
domicilio varchar(50),
dni varchar(50),
numeromovil int (20),
tipocliente varchar(50));
--
create table if not exists reservas(

    idreserva int auto_increment primary key,
    fechainicioreserva date,
    fechafinreserva date,
    idcliente int (50) not null,
    idhabitacion int (50) not null
);
--
create table if not exists habitaciones(
    idhabitacion int auto_increment primary key,
    numerohabitacion int not null UNIQUE,
    tipo varchar(50) not null,
    precionoche float (20) not null,
    vistas varchar(50) not null,
    extras varchar(50) not null


);
--
alter table reservas

    add foreign key(idcliente) references clientes(idcliente),
    add foreign key(idhabitacion) references habitaciones(idhabitacion);
--
delimiter ||
create function existeDni(f_dni varchar(20))
returns bit
begin
    declare i int;
    set i = 0;
    while ( i < (select max(idcliente) from clientes)) do
    if  ((select dni from clientes where idcliente = (i + 1)) like f_dni) then return 1;
    end if;
    set i = i + 1;
    end while;
    return 0;
end; ||
delimiter ;
--
delimiter ||
create function existeReservaId(f_id int(20))
returns bit
begin
    declare i int;
    set i = 0;
    while ( i < (select max(idreserva) from reservas)) do
    if  ((select idreserva from reservas where idreserva = (i + 1)) like f_id) then return 1;
    end if;
    set i = i + 1;
    end while;
    return 0;
end; ||
delimiter ;
--
delimiter ||
create function existeNumeroHabitacion(f_numerohabitacion int(20))
returns bit
begin
    declare i int;
    set i = 0;
    while ( i < (select max(idhabitacion) from habitaciones)) do
    if  ((select numerohabitacion from habitaciones where idhabitacion = (i + 1)) like f_numerohabitacion) then return 1;
    end if;
    set i = i + 1;
    end while;
    return 0;
end; ||
delimiter ;
--
/**
  Procedimiento para crear la tabla clientes con sus correspondientes apartados.
 */

DELIMITER //
CREATE PROCEDURE crearTablaClientes()
BEGIN
    CREATE TABLE if not exists clientes(
               idcliente int auto_increment primary key,
                nombre varchar(50) not null,
                apellidos varchar(50) not null,
                fechanacimiento date,
                domicilio varchar(50),
                dni varchar(50),
                numeromovil int (20),
                tipocliente varchar(50));
END //
--

/**
  Procedimiento para crear la tabla clientes con sus correspondientes apartados.
 */

DELIMITER //
CREATE PROCEDURE crearTablaReservas()
BEGIN
    CREATE TABLE if not exists reservas(
                idreserva int auto_increment primary key,
                fechainicioreserva date,
                fechafinreserva date,
                idcliente int (50) not null,
                idhabitacion int (50) not null);
    alter table reservas

    add foreign key(idcliente) references clientes(idcliente),
    add foreign key(idhabitacion) references habitaciones(idhabitacion);
--
END //
--
/**
  Procedimiento para crear la tabla clientes con sus correspondientes apartados.
 */

DELIMITER //
CREATE PROCEDURE crearTablaHabitaciones()
BEGIN
    CREATE TABLE if not exists habitaciones(
                idhabitacion int auto_increment primary key,
                numerohabitacion int not null UNIQUE,
                tipo varchar(50) not null,
                precionoche float (20) not null,
                vistas varchar(50) not null,
                extras varchar(50) not null);
END //
--
/**
  Procedimiento para crear la tabla clientes con sus correspondientes apartados.
 */

DELIMITER //
CREATE PROCEDURE crearBaseDatos()
BEGIN
    CREATE DATABASE if not exists datoshotel;
   
END //
--