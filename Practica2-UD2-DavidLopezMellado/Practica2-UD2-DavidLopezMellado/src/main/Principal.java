/**
 * @author David López Mellado
 * @since 1.8
 * @version 1.0
 */
package main;

import gui.Controlador;
import gui.Modelo;
import gui.Vista;

public class Principal {

    public static void main(String[] args) {
        /**
         * Método que nos crea un objeto de la clase Modelo y
         * otro de la clase Vista
         * y nos lo añade a un objeto creado de la clase Controlador.
         */
        Modelo modelo = new Modelo();
        Vista vista = new Vista();
        Controlador controlador = new Controlador(modelo, vista);
    }
}
