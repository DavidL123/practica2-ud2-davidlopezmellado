/**
 * @author David López Mellado
 * @since 1.8
 * @version 1.0
 */
package base.enums;


/**
 * Esta clase enum enumera las constantes con
 * las que rellenaremos el JComboBox comboTipoCliente
 * de la vista.
 * Representa los tipos de clientes más característicos
 * que tenemos.
 */
public enum TiposClientes {


    VERANO("Verano"),
    INVIERNO("Invierno"),
    PRIMAVERA("Primavera"),
    OTONO("Otoño");


    private String valor;

    TiposClientes(String valor){

        this.valor=valor;
    }

    public String getValor(){

        return valor;

    }


}
