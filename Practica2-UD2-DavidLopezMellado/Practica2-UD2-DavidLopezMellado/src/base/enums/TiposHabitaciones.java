/**
 * @author David López Mellado
 * @since 1.8
 * @version 1.0
 */
package base.enums;
/**
 * Esta clase enum enumera las constantes con
 * las que rellenaremos el JComboBox comboTipoHabitacion     * de la vista.
 * Representa los tipos de habitaciones que
 * tenemos.
 */
public enum TiposHabitaciones {


    GRANDE("Grande"),
    PEQUENA("Pequeña"),
    FAMILIAR("Familiar"),
    DOBLE("Doble");

    private String valor;


    TiposHabitaciones(String valor){

        this.valor= valor;
    }


    public String getValor(){


        return valor;
    }

}
