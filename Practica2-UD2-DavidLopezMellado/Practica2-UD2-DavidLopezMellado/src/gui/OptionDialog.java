/**
 * @author David López Mellado
 * @since 1.8
 * @version 1.0
 */
package gui;

import javax.swing.*;

import java.awt.*;

public class OptionDialog extends JDialog{
    private JPanel panel1;
    JButton btnOpcionesGuardar;
    JTextField tfIP;
    JTextField tfUser;
    JPasswordField pfPass;
    JPasswordField pfAdmin;

    private Frame owner;


    /**
     * Constructor de la clase.
     * @param owner Propietario del dialog.
     */

    public OptionDialog(Frame owner){
        super(owner,"Opciones",true);
        this.owner=owner;
        initDialog();
    }


    /**
     * Inicializa el JDialog
     */

    private void initDialog(){
        this.setContentPane(panel1);
        this.panel1.setBorder(BorderFactory.createEmptyBorder(20, 20, 20, 20));
        this.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
        this.pack();
        this.setSize(new Dimension(this.getWidth()+200, this.getHeight()));
        this.setLocationRelativeTo(owner);
    }



}
