/**
 * @author David López Mellado
 * @since 1.8
 * @version 1.0
 */
package gui;

import java.io.*;
import java.sql.*;
import java.time.LocalDate;
import java.util.Properties;

public class Modelo {


    private String ip;
    private String user;
    private String password;
    private String adminPassword;

    /**
     * Constructor de la clase, inicializa los ArrayLists,
     * carga los datos del fichero properties y setea isChanged a false.
     */
    public Modelo() {
        getPropValues();
    }

    String getIP() {
        return ip;
    }
    String getUser() {
        return user;
    }
    String getPassword() {
        return password;
    }
    String getAdminPassword() {
        return adminPassword;
    }


    private Connection conexion;


    /**
     * Método que conecta con la base de datos y si no lo consigue,
     * carga los datos de un fichero.
     */
    public void conectar() {

        try {
            conexion = DriverManager.getConnection(
                    "jdbc:mysql://"+ip+":3306/datoshotel",user, password);
        } catch (SQLException sqle) {
            try {
                conexion = DriverManager.getConnection(
                        "jdbc:mysql://"+ip+":3306/",user, password);

                PreparedStatement statement = null;

                String code = leerFichero();
                String[] query = code.split("--");
                for (String aQuery : query) {
                    statement = conexion.prepareStatement(aQuery);
                    statement.executeUpdate();
                }
                assert statement != null;
                statement.close();

            } catch (SQLException | IOException e) {
                e.printStackTrace();
            }
        }
    }


    /**
     * Método que nos sirve para leer un fichero que le hemos indicado.
     * @return
     * @throws IOException
     */

    private String leerFichero() throws IOException {
        try (BufferedReader reader = new BufferedReader(new FileReader("baseDatosHotel_java.sql"))) {
            String linea;
            StringBuilder stringBuilder = new StringBuilder();
            while ((linea = reader.readLine()) != null) {
                stringBuilder.append(linea);
                stringBuilder.append(" ");
            }

            return stringBuilder.toString();
        }
    }


    /**
     * Método que nos permite desconectar la conexión con la base de datos.
     */
    public void desconectar() {
        try {
            conexion.close();
            conexion = null;
        } catch (SQLException sqle) {
            sqle.printStackTrace();
        }
    }


    /**
     * Método que usaremos para recibir los parámetros que necesistaremos
     * para poder realizar un sentencia para añadir un nuevo cliente,
     * lo haremos de forma segura, para evitar inyecciones de tipo sql.
     * @param nombre
     * @param apellidos
     * @param fechaNacimiento
     * @param domicilio
     * @param dni
     * @param numeroMovil
     * @param tipoCliente
     */

    public void insertarCliente(String nombre, String apellidos, LocalDate fechaNacimiento,
                                String domicilio, String dni, int numeroMovil, String tipoCliente){

        String sentenciaSql = "INSERT INTO clientes (nombre, apellidos, fechanacimiento, " +
                "domicilio, dni, numeromovil,tipocliente) VALUES (?, ?, ?, ?, ?, ?, ?)";
        PreparedStatement sentencia = null;

        try {
            sentencia = conexion.prepareStatement(sentenciaSql);
            sentencia.setString(1, nombre);
            sentencia.setString(2, apellidos);
            sentencia.setDate(3, Date.valueOf(fechaNacimiento));
            sentencia.setString(4, domicilio);
            sentencia.setString(5, dni);
            sentencia.setInt(6, numeroMovil);
            sentencia.setString(7, tipoCliente);
            sentencia.executeUpdate();
        } catch (SQLException sqle) {
            sqle.printStackTrace();
        } finally {
            if (sentencia != null)
                try {
                    sentencia.close();
                } catch (SQLException sqle) {
                    sqle.printStackTrace();
                }
        }

    }


    /**
     * Método que usaremos para recibir los parámetros que necesistaremos
     * para poder realizar un sentencia para añadir una nueva reserva,
     * lo haremos de forma segura, para evitar inyecciones de tipo sql.
     * @param fechaInicioReserva
     * @param fechaFinReserva
     * @param cliente
     * @param habitacion
     */

    public void insertarReserva(LocalDate fechaInicioReserva, LocalDate fechaFinReserva, String cliente,
                                String habitacion){

        String sentenciaSql = "INSERT INTO reservas (fechainicioreserva, fechafinreserva, idcliente, " +
                "idhabitacion) VALUES (?, ?, ?, ?)";
        PreparedStatement sentencia = null;

        int idcliente = Integer.valueOf(cliente.split(" ")[0]);
        int idhabitacion = Integer.valueOf(cliente.split(" ")[0]);


        try {
            sentencia = conexion.prepareStatement(sentenciaSql);
            sentencia.setDate(1, Date.valueOf(fechaInicioReserva));
            sentencia.setDate(2, Date.valueOf(fechaFinReserva));
            sentencia.setInt(3, idcliente);
            sentencia.setInt(4, idhabitacion);
            sentencia.executeUpdate();
        } catch (SQLException sqle) {
            sqle.printStackTrace();
        } finally {
            if (sentencia != null)
                try {
                    sentencia.close();
                } catch (SQLException sqle) {
                    sqle.printStackTrace();
                }
        }

    }


    /**
     * Método que usaremos para recibir los parámetros que necesistaremos
     * para poder realizar un sentencia para añadir un nuevo cliente,
     * lo haremos de forma segura, para evitar inyecciones de tipo sql.
     * @param numero
     * @param tipoHabitacion
     * @param precioHabitacionPorNoche
     * @param vistas
     * @param extras
     */

    public void insertarHabitacion(int numero, String tipoHabitacion, float precioHabitacionPorNoche,
                                String vistas, String extras){

        String sentenciaSql = "INSERT INTO habitaciones (numerohabitacion, tipo, precionoche, " +
                "vistas, extras) VALUES (?, ?, ?, ?, ?)";
        PreparedStatement sentencia = null;

        try {
            sentencia = conexion.prepareStatement(sentenciaSql);
            sentencia.setInt(1, numero);
            sentencia.setString(2, tipoHabitacion);
            sentencia.setFloat(3, precioHabitacionPorNoche);
            sentencia.setString(4, vistas);
            sentencia.setString(5, extras);
            sentencia.executeUpdate();
        } catch (SQLException sqle) {
            sqle.printStackTrace();
        } finally {
            if (sentencia != null)
                try {
                    sentencia.close();
                } catch (SQLException sqle) {
                    sqle.printStackTrace();
                }
        }

    }

    /**
     * Método que recibirá una serie de parámetros y nos permitirá modificar
     * un registro de la tabla cliente indicándole el id del registro.
     * @param nombre
     * @param apellidos
     * @param fechaNacimiento
     * @param domicilio
     * @param dni
     * @param numeroMovil
     * @param tipoCliente
     * @param idcliente
     */

    public void modificarCliente(String nombre, String apellidos, LocalDate fechaNacimiento, String domicilio,
                                 String dni, int numeroMovil, String tipoCliente, int idcliente){

        String sentenciaSql = "UPDATE clientes SET nombre = ?, apellidos = ?, fechanacimiento = ?, domicilio = ?, dni = ?" +
                ", numeromovil = ?, tipocliente = ? WHERE idcliente = ?";
        PreparedStatement sentencia = null;

        try {
            sentencia = conexion.prepareStatement(sentenciaSql);
            sentencia.setString(1, nombre);
            sentencia.setString(2, apellidos);
            sentencia.setDate(3, Date.valueOf(fechaNacimiento));
            sentencia.setString(4, domicilio);
            sentencia.setString(5, dni);
            sentencia.setInt(6, numeroMovil);
            sentencia.setString(7, tipoCliente);
            sentencia.setInt(8, idcliente);
            sentencia.executeUpdate();
        } catch (SQLException sqle) {
            sqle.printStackTrace();
        } finally {
            if (sentencia != null)
                try {
                    sentencia.close();
                } catch (SQLException sqle) {
                    sqle.printStackTrace();
                }
        }
    }


    /**
     *   Método que recibirá una serie de parámetros y nos permitirá modificar
     *   un registro de la tabla reserva indicándole el id del registro.
     * @param fechaInicioReserva
     * @param fechaFinReserva
     * @param cliente
     * @param habitacion
     * @param idreserva
     */
    public void modificarReserva(LocalDate fechaInicioReserva, LocalDate fechaFinReserva, String cliente, String habitacion,
                                 int idreserva){

        String sentenciaSql = "UPDATE reservas SET fechainicioreserva = ?, fechafinreserva = ?, idcliente = ?, idhabitacion = ?" +
                " WHERE idreserva = ?";
        PreparedStatement sentencia = null;

        int idcliente=Integer.valueOf(cliente.split(" ")[0]);
        int idhabitacion=Integer.valueOf(habitacion.split(" ")[0]);


        try {
            sentencia = conexion.prepareStatement(sentenciaSql);
            sentencia.setDate(1, Date.valueOf(fechaInicioReserva));
            sentencia.setDate(2, Date.valueOf(fechaFinReserva));
            sentencia.setInt(3, idcliente);
            sentencia.setInt(4, idhabitacion);
            sentencia.setInt(5, idreserva);
            sentencia.executeUpdate();
        } catch (SQLException sqle) {
            sqle.printStackTrace();
        } finally {
            if (sentencia != null)
                try {
                    sentencia.close();
                } catch (SQLException sqle) {
                    sqle.printStackTrace();
                }
        }
    }


    /**
     *  Método que recibirá una serie de parámetros y nos permitirá modificar
     *  un registro de la tabla habitación indicándole el id del registro.
     * @param numeroHabitacion
     * @param tipoHabitacion
     * @param precioHabitacionPorNoche
     * @param vistas
     * @param extras
     * @param idhabitacion
     */

    public void modificarHabitacion(int numeroHabitacion, String tipoHabitacion, float precioHabitacionPorNoche, String vistas,
                                 String extras, int idhabitacion){

        String sentenciaSql = "UPDATE habitaciones SET numerohabitacion = ?, tipo = ?, precionoche = ?, vistas = ?" +
                ", extras = ? WHERE idhabitacion = ?";
        PreparedStatement sentencia = null;



        try {
            sentencia = conexion.prepareStatement(sentenciaSql);
            sentencia.setInt(1,numeroHabitacion);
            sentencia.setString(2, tipoHabitacion);
            sentencia.setFloat(3, precioHabitacionPorNoche);
            sentencia.setString(4, vistas);
            sentencia.setString(5, extras);
            sentencia.setInt(6, idhabitacion);
            sentencia.executeUpdate();
        } catch (SQLException sqle) {
            sqle.printStackTrace();
        } finally {
            if (sentencia != null)
                try {
                    sentencia.close();
                } catch (SQLException sqle) {
                    sqle.printStackTrace();
                }
        }
    }


    /**
     * Método que nos permitirá borrar un registro de la clase cliente
     * indicándo cual es su id.
     * @param idcliente
     */
    public void borrarCliente(int idcliente) {
        String sentenciaSql = "DELETE FROM clientes WHERE idcliente = ?";
        PreparedStatement sentencia = null;

        try {
            sentencia = conexion.prepareStatement(sentenciaSql);
            sentencia.setInt(1, idcliente);
            sentencia.executeUpdate();
        } catch (SQLException sqle) {
            sqle.printStackTrace();
        } finally {
            if (sentencia != null)
                try {
                    sentencia.close();
                } catch (SQLException sqle) {
                    sqle.printStackTrace();
                }
        }
    }

    /**
     * Método que nos permitirá borrar un registro de la clase reserva
     *      * indicándo cual es su id.
     * @param idreserva
     */

    public void borrarReserva(int idreserva) {
        String sentenciaSql = "DELETE FROM reservas WHERE idreserva = ?";
        PreparedStatement sentencia = null;

        try {
            sentencia = conexion.prepareStatement(sentenciaSql);
            sentencia.setInt(1, idreserva);
            sentencia.executeUpdate();
        } catch (SQLException sqle) {
            sqle.printStackTrace();
        } finally {
            if (sentencia != null)
                try {
                    sentencia.close();
                } catch (SQLException sqle) {
                    sqle.printStackTrace();
                }
        }
    }


    /**
     * Método que nos permitirá borrar un registro de la clase habitación
     *  indicándo cual es su id.
     * @param idhabitacion
     */
    public void borrarHabitacion(int idhabitacion) {

        String sentenciaSql = "DELETE FROM habitaciones WHERE idhabitacion = ?";
        PreparedStatement sentencia = null;

        try {
            sentencia = conexion.prepareStatement(sentenciaSql);
            sentencia.setInt(1, idhabitacion);
            sentencia.executeUpdate();
        } catch (SQLException sqle) {
            sqle.printStackTrace();
        } finally {
            if (sentencia != null)
                try {
                    sentencia.close();
                } catch (SQLException sqle) {
                    sqle.printStackTrace();
                }
        }
    }


    /**
     * Método que nos lee el archivo de propiedades y setea los atributos
     * pertinentes.
     */
    private void getPropValues() {
        InputStream inputStream = null;
        try {
            Properties prop = new Properties();
            String propFileName = "config.properties";

            inputStream = new FileInputStream(propFileName);

            prop.load(inputStream);
            ip = prop.getProperty("ip");
            user = prop.getProperty("user");
            password = prop.getProperty("pass");
            adminPassword = prop.getProperty("admin");

        } catch (Exception e) {
            System.out.println("Exception: " + e);
        } finally {
            try {
                if (inputStream != null) inputStream.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * Actualiza las propiedades pasadas por parámetro del archivo de propiedades
     *
     * @param ip   ip de la bbdd
     * @param user user de la bbdd
     * @param pass contraseña de la bbdd
     * @param adminPass contraseña del administrador
     */

    void setPropValues(String ip, String user, String pass, String adminPass) {
        try {
            Properties prop = new Properties();
            prop.setProperty("ip", ip);
            prop.setProperty("user", user);
            prop.setProperty("pass", pass);
            prop.setProperty("admin", adminPass);
            OutputStream out = new FileOutputStream("config.properties");
            prop.store(out, null);

        } catch (IOException ex) {
            ex.printStackTrace();
        }
        this.ip = ip;
        this.user = user;
        this.password = pass;
        this.adminPassword = adminPass;
    }


    /**
     * Este metodo devuelve los registros de la tabla clientes guardadas en la base de datos
     * @return consulta
     * @throws SQLException
     */
    ResultSet consultarCliente() throws SQLException {
        String sentenciaSql = "SELECT concat(idcliente) as 'ID', concat(nombre) as 'Nombre cliente', concat(apellidos) as 'Apellidos cliente', " +
                "concat(fechanacimiento) as 'Fecha nacimiento', concat(domicilio) as 'Domicilio', concat(dni) as 'DNI'," +
                " concat(numeromovil) as 'Número de movil', concat(tipocliente) as 'Tipo de cliente' FROM clientes";
        PreparedStatement sentencia = null;
        ResultSet resultado = null;
        sentencia = conexion.prepareStatement(sentenciaSql);
        resultado = sentencia.executeQuery();
        return resultado;
    }

    /**
     * Este metodo devuelve las habitaciones guardados en la bbdd
     * @return consulta
     * @throws SQLException
     */
    ResultSet consultarHabitacion() throws SQLException {
        String sentenciaSql = "SELECT concat(idhabitacion) as 'ID', concat(numerohabitacion) as 'Número de habitación', " +
                "concat(tipo) as 'Tipo de habitación', " +
                "concat(precionoche) as 'Precio por noche', concat(vistas) as 'Vistas', concat(extras) as 'Extras' FROM habitaciones";
        PreparedStatement sentencia = null;
        ResultSet resultado = null;
        sentencia = conexion.prepareStatement(sentenciaSql);
        resultado = sentencia.executeQuery();
        return resultado;
    }

    /**
     * Este metodo devuelve todas las reservas de la base de datos
     * @return consulta
     * @throws SQLException
     */
    ResultSet consultarReserva() throws SQLException {
        String sentenciaSql = "SELECT concat(r.idreserva) as 'ID', concat(r.fechainicioreserva) as 'Fecha inicio reserva', " +
                "concat(r.fechafinreserva) as 'Fecha fin reserva', " +
                "concat(c.idcliente) as 'Id cliente', concat(h.idhabitacion) as 'Id habitación' " +
                " FROM reservas as r " +
                "inner join clientes as c on c.idcliente = r.idcliente inner join " +
                "habitaciones as h on h.idhabitacion = r.idhabitacion";
        PreparedStatement sentencia = null;
        ResultSet resultado = null;
        sentencia = conexion.prepareStatement(sentenciaSql);
        resultado = sentencia.executeQuery();
        return resultado;
    }


    /**
     * Método que nos devuelve los registros obtenidos al realizar la consulta
     * dentro del método. (Select...)
     * @return
     * @throws SQLException
     */
    public ResultSet obtenerDatos1() throws SQLException {
        if (conexion==null) {
            return null;
        }
        if (conexion.isClosed()) {
            return null;
        }
        String consulta ="SELECT tipo,COUNT(*) FROM habitaciones GROUP BY tipo";
        PreparedStatement sentencia = null;
        sentencia=conexion.prepareStatement(consulta);
        ResultSet resultado=sentencia.executeQuery();
        return resultado;
    }


    /**
     * Método que nos devuelve los registros obtenidos al realizar la consulta
     * dentro del método. (Select...)
     * @return
     * @throws SQLException
     */
    public ResultSet obtenerDatos2() throws SQLException {
        System.out.println("hola");
        if (conexion==null) {
            return null;
        }
        if (conexion.isClosed()) {
            return null;
        }
        String consulta ="select * from reservas where MONTHNAME(fechafinreserva)=MONTHNAME(CURRENT_DATE) AND YEAR(fechafinreserva) = YEAR(CURRENT_DATE)";
        PreparedStatement sentencia = null;
        sentencia=conexion.prepareStatement(consulta);
        ResultSet resultado=sentencia.executeQuery();
        return resultado;
    }

    /**
     * Este metodo comprueba si existe un registro con el mismo DNI introducido.
     * Devuelve un valor booleano.
     *
     * @param isbn
     * @return
     */
    public boolean clienteDniYaExiste(String isbn) {
        String clienteIdConsult = "SELECT existeDni(?)";
        PreparedStatement function;
        boolean dniExists = false;
        try {
            function = conexion.prepareStatement(clienteIdConsult);
            function.setString(1, isbn);
            ResultSet rs = function.executeQuery();
            rs.next();

            dniExists = rs.getBoolean(1);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return dniExists;
    }


    /**
     * Este metodo comprueba si ya existe una habitación el mismo número.
     * @param numeroHabitacion
     * @return
     */
    public boolean habitacionNumeroYaExiste(int numeroHabitacion) {
        String habitacionNumeroConsult = "SELECT existeNumeroHabitacion(?)";
        PreparedStatement function;
        boolean numeroExists = false;
        try {
            function = conexion.prepareStatement(habitacionNumeroConsult);
            function.setInt(1, numeroHabitacion);
            ResultSet rs = function.executeQuery();
            rs.next();

            numeroExists = rs.getBoolean(1);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return numeroExists;
    }

    /**
     * Método que recibe el nombre buscado y nos permitirá buscar si existe a través de una consulta que se ejecutará
     * @param nombreBuscado Nombre buscado del cliente
     * @return
     * @throws SQLException
     */
    public ResultSet buscarPorNombre(String nombreBuscado) throws SQLException{
        if(conexion==null){
            return null;
        }
        if(conexion.isClosed()){
            return null;
        }
        String consulta ="SELECT * FROM clientes WHERE nombre=?";
        PreparedStatement sentencia = null;
        sentencia=conexion.prepareStatement(consulta);
        sentencia.setString(1,nombreBuscado);
        ResultSet resultado=sentencia.executeQuery();
        return resultado;

    }

    /**
     * Método que nos conectará con la base de datos y ejecutará el procedimiento crearTablaCLientes()
     * @throws SQLException
     */
    public void crearTablaClientes() throws SQLException{
        conexion=null;
        conexion= DriverManager.getConnection(
                "jdbc:mysql://"+ip+":3306/datoshotel",user, password);

        String sentenciaSql="call crearTablaClientes()";
        CallableStatement procedimiento=null;
        procedimiento=conexion.prepareCall(sentenciaSql);
        procedimiento.execute();
    }

    /**
     * Método que nos conectará con la base de datos y ejecutará el procedimiento crearTablaCLientes()
     * @throws SQLException
     */
    public void crearTablaReservas() throws SQLException{
        conexion=null;
        conexion= DriverManager.getConnection(
                "jdbc:mysql://"+ip+":3306/datoshotel",user, password);

        String sentenciaSql="call crearTablaReservas()";
        CallableStatement procedimiento=null;
        procedimiento=conexion.prepareCall(sentenciaSql);
        procedimiento.execute();
    }

    /**
     * Método que nos conectará con la base de datos y ejecutará el procedimiento crearTablaCLientes()
     * @throws SQLException
     */
    public void crearTablaHabitaciones() throws SQLException{
        conexion=null;
        conexion= DriverManager.getConnection(
                "jdbc:mysql://"+ip+":3306/datoshotel",user, password);

        String sentenciaSql="call crearTablaHabitaciones()";
        CallableStatement procedimiento=null;
        procedimiento=conexion.prepareCall(sentenciaSql);
        procedimiento.execute();
    }

    /**
     * Método que nos conectará con la base de datos y ejecutará el procedimiento crearTablaCLientes()
     * @throws SQLException
     */
    public void crearBaseDatos() throws SQLException{
        conexion=null;
        conexion= DriverManager.getConnection(
                "jdbc:mysql://"+ip+":3306",user, password);

        String sentenciaSql="call crearBaseDatos()";
        CallableStatement procedimiento=null;
        procedimiento=conexion.prepareCall(sentenciaSql);
        procedimiento.execute();
    }

}
