/**
 * @author David López Mellado
 * @since 1.8
 * @version 1.0
 */
package gui;

import com.github.lgooddatepicker.components.DateTimePicker;
import util.Util;

import javax.swing.*;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.table.DefaultTableModel;
import java.awt.event.*;
import java.sql.Date;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.Vector;

public class Controlador implements ActionListener, ItemListener, ListSelectionListener, WindowListener {
    private Modelo modelo;
    private Vista vista;
    boolean refrescar;


    /**
     * Constructor de la clase, inicialia el modelo
     * y la vista y
     * añade los listeners
     *
     * @param modelo Modelo
     * @param vista Vista
     */




    public Controlador(Modelo modelo, Vista vista){
        this.modelo=modelo;
        this.vista=vista;
        //modelo.conectar();
        setOptions();
        addActionListeners(this);
        addItemListeners(this);
        addWindowListeners(this);
        //refrescarTodo();
        iniciarTablaBuscar();
    }


    /**
     *Nos ejecuta los métodos
     *         refrescarClientes();
     *         refrescarHabitacion();
     *         refrescarReservas();
     *  y le da valor false a la variable
     *  refrescar.
     *
     */


    private void refrescarTodo(){
        refrescarClientes();
        refrescarHabitacion();
        refrescarReservas();
        refrescar=false;
    }


    /**
     * Añade ActionListeners a los botones y menus de la vista
     * @param listener ActionListener que se añade
     */


    private void addActionListeners(ActionListener listener){

        vista.btnClientesAnadir.addActionListener(listener);
        vista.btnClientesModificar.addActionListener(listener);
        vista.btnClientesEliminar.addActionListener(listener);
        vista.btnReservasAnadir.addActionListener(listener);
        vista.btnReservasModificar.addActionListener(listener);
        vista.btnReservasEliminar.addActionListener(listener);
        vista.btnReservasMostrarMes.addActionListener(listener);
        vista.btnHabitacionesAnadir.addActionListener(listener);
        vista.btnHabitacionesModificar.addActionListener(listener);
        vista.btnHabitacionesEliminar.addActionListener(listener);
        vista.btnHabitacionCantidadPorTipo.addActionListener(listener);
        vista.optionDialog.btnOpcionesGuardar.addActionListener(listener);
        vista.itemOpciones.addActionListener(listener);
        vista.itemSalir.addActionListener(listener);
        vista.btnValidate.addActionListener(listener);
        vista.btnBuscarNombre.addActionListener(listener);
        vista.itemConectar.addActionListener(listener);
        vista.itemCrearBaseDatos.addActionListener(listener);
        vista.itemCrearTablaClientes.addActionListener(listener);
        vista.itemCrearTablaReservas.addActionListener(listener);
        vista.itemCrearTablaHabitaciones.addActionListener(listener);
        vista.btnMostrarTodasReservas.addActionListener(listener);


    }

    /**
     * Añade WindowListeners a la vista
     * @param listener WindowListener que se añade
     */

    private void addWindowListeners(WindowListener listener){ vista.addWindowListener(listener);}


    /**
     * Método que nos muestra los atributos de un objeto seleccionado
     * y los borra una vez se deselecciona.
     * @param e Evento producido en una lista.
     */
    @Override
    public void valueChanged(ListSelectionEvent e){
        if(e.getValueIsAdjusting()
            && !((ListSelectionModel) e.getSource()).isSelectionEmpty()){
            if(e.getSource().equals(vista.clientesTabla.getSelectionModel())){
                //duda de que hace
                int row = vista.clientesTabla.getSelectedRow();
                vista.txtNombreCliente.setText(String.valueOf(vista.clientesTabla.getValueAt(row, 1)));
                vista.txtApellidosCliente.setText(String.valueOf(vista.clientesTabla.getValueAt(row, 2)));
                vista.fechaNacimientoCliente.setDate((Date.valueOf(String.valueOf(vista.clientesTabla.getValueAt(row, 3)))).toLocalDate());
                vista.txtDomicilioCliente.setText(String.valueOf(vista.clientesTabla.getValueAt(row,4)));
                vista.txtDNICliente.setText(String.valueOf(vista.clientesTabla.getValueAt(row,5)));
                vista.txtNumeroMovilCliente.setText(String.valueOf(vista.clientesTabla.getValueAt(row,6)));
                vista.comboTipoCliente.setSelectedItem(String.valueOf(vista.clientesTabla.getValueAt(row,7)));

            }else if(e.getSource().equals(vista.reservaTabla.getSelectionModel())){
                int row=vista.reservaTabla.getSelectedRow();
                vista.fechaInicioReserva.setDate((Date.valueOf(String.valueOf(vista.reservaTabla.getValueAt(row, 1)))).toLocalDate());
                vista.fechaFinReserva.setDate((Date.valueOf(String.valueOf(vista.reservaTabla.getValueAt(row, 2)))).toLocalDate());
                vista.comboClientes.setSelectedItem(String.valueOf(vista.reservaTabla.getValueAt(row, 3)));
                vista.comboHabitaciones.setSelectedItem(String.valueOf(vista.reservaTabla.getValueAt(row,4)));
            }else if(e.getSource().equals(vista.habitacionTabla.getSelectionModel())){
                int row=vista.habitacionTabla.getSelectedRow();
                vista.txtNumeroHabitacion.setText(String.valueOf(vista.habitacionTabla.getValueAt(row,1)));
                vista.comboTipoHabitacion.setSelectedItem(String.valueOf(vista.habitacionTabla.getValueAt(row,2)));
                vista.txtPrecioHabitacionPorNoche.setText(String.valueOf(vista.habitacionTabla.getValueAt(row,3)));
                vista.txtVistas.setText(String.valueOf(vista.habitacionTabla.getValueAt(row,4)));
                vista.txtExtras.setText(String.valueOf(vista.habitacionTabla.getValueAt(row,5)));

            }else if(e.getValueIsAdjusting()
                && ((ListSelectionModel) e.getSource()).isSelectionEmpty() && !refrescar){
                if(e.getSource().equals(vista.clientesTabla.getSelectionModel())){
                    borrarCamposClientes();
                }else if(e.getSource().equals(vista.reservaTabla.getSelectionModel())){
                    borrarCamposReservas();
                }else if(e.getSource().equals(vista.habitacionTabla.getSelectionModel())){
                    borrarCamposHabitaciones();
                }
            }

        }
    }


    /**
     * Indica que las órdenes que tiene que realizar
     * cada botón que tenemos de cada pestaña.
     * Serían los botones añaadir, modificar y eliminar.
     * También los botones del ménu y del dialog de opciones.
     *
     * @param e Evento producido al pulsar un botón o menu
     */

    @Override
    public void actionPerformed(ActionEvent e){
        String command = e.getActionCommand();
        switch (command){
            case "Opciones":
                vista.adminPasswordDialog.setVisible(true);
                break;
            case "Conectar":
                modelo.conectar();
                iniciarTablaBuscar();
                iniciarTablaCantidadPorTipo();
                refrescarTodo();
                break;
            case "Desconectar":
                modelo.desconectar();
                break;
            case "Salir":
                System.exit(0);
                break;
            case "abrirOpciones":
                if(String.valueOf(vista.adminPassword.getPassword()).equals(modelo.getAdminPassword())) {
                    vista.adminPassword.setText("");
                    vista.adminPasswordDialog.dispose();
                    vista.optionDialog.setVisible(true);
                } else {
                    Util.showErrorAlert("La contraseña introducida no es correcta.");
                }
                break;
            case "guardarOpciones":
                modelo.setPropValues(vista.optionDialog.tfIP.getText(), vista.optionDialog.tfUser.getText(),
                        String.valueOf(vista.optionDialog.pfPass.getPassword()), String.valueOf(vista.optionDialog.pfAdmin.getPassword()));
                vista.optionDialog.dispose();
                vista.dispose();
                new Controlador(new Modelo(), new Vista());
                break;
            case "CrearBaseDatos":
                try{
                    modelo.crearBaseDatos();

                }catch (SQLException e1){
                    e1.printStackTrace();
                }
                break;
            case "CrearTablaClientes":
                try{
                    modelo.crearTablaClientes();

                }catch (SQLException e1){
                    e1.printStackTrace();
                }
                break;
            case "CrearTablaReservas":
                try{
                    modelo.crearTablaReservas();

                }catch (SQLException e1){
                    e1.printStackTrace();
                }
                break;
            case "CrearTablaHabitaciones":
                try{
                    modelo.crearTablaHabitaciones();

                }catch (SQLException e1){
                    e1.printStackTrace();
                }
                break;
            case "cantidadPorTipo":

                try {
                    cargarFilas1(modelo.obtenerDatos1());
                } catch (SQLException ex) {
                    ex.printStackTrace();
                }

                break;
            case "mostrarReservaMes":

                try {
                    cargarFilas2(modelo.obtenerDatos2());
                } catch (SQLException ex) {
                    ex.printStackTrace();
                }

                break;
            case "mostrarTodasReservas":
                refrescarReservas();

                break;
            case "anadirCliente":{
                try {
                    if (comprobarClienteVacio()) {
                        Util.showErrorAlert("Rellena todos los campos");
                        vista.clientesTabla.clearSelection();
                    } else if (modelo.clienteDniYaExiste(vista.txtDNICliente.getText())) {
                        Util.showErrorAlert("Ese DNI ya existe.\nIntroduce un cliente diferente");
                        vista.clientesTabla.clearSelection();
                    } else {
                        modelo.insertarCliente(
                                vista.txtNombreCliente.getText(),
                                vista.txtApellidosCliente.getText(),
                                vista.fechaNacimientoCliente.getDate(),
                                vista.txtDomicilioCliente.getText(),
                                vista.txtDNICliente.getText(),
                                Integer.parseInt(vista.txtNumeroMovilCliente.getText()),
                                String.valueOf(vista.comboTipoCliente.getSelectedItem()));
                    }
                } catch (NumberFormatException nfe) {
                    Util.showErrorAlert("Introduce números en los campos que lo requieren");
                    vista.clientesTabla.clearSelection();
                }
                borrarCamposClientes();
                refrescarClientes();
            }
            break;
            case "modificarCliente": {
                try {
                    if (comprobarClienteVacio()) {
                        Util.showErrorAlert("Rellena todos los campos");
                        vista.clientesTabla.clearSelection();
                    } else {
                        modelo.modificarCliente(
                                vista.txtNombreCliente.getText(),
                                vista.txtApellidosCliente.getText(),
                                vista.fechaNacimientoCliente.getDate(),
                                vista.txtDomicilioCliente.getText(),
                                vista.txtDNICliente.getText(),
                                Integer.parseInt(vista.txtNumeroMovilCliente.getText()),
                                String.valueOf(vista.comboTipoCliente.getSelectedItem()),
                                Integer.parseInt((String)vista.clientesTabla.getValueAt(vista.clientesTabla.getSelectedRow(), 0)));
                    }
                } catch (NumberFormatException nfe) {
                    Util.showErrorAlert("Introduce números en los campos que lo requieren");
                    vista.clientesTabla.clearSelection();
                }
                borrarCamposClientes();
                refrescarClientes();
            }
            break;
            case "eliminarCliente":
                modelo.borrarCliente(Integer.parseInt((String)vista.clientesTabla.getValueAt(vista.clientesTabla.getSelectedRow(), 0)));
                borrarCamposClientes();
                refrescarClientes();
                break;
            case "anadirReserva": {
                try {
                    if (comprobarReservaVacio()) {
                        Util.showErrorAlert("Rellena todos los campos");
                        vista.reservaTabla.clearSelection();/*
                    } else if (modelo.reservaIdYaExiste()) {
                        Util.showErrorAlert("Esa reserva ya existe.\nIntroduce una reserva diferente");
                        vista.reservaTabla.clearSelection();*/
                    } else {
                        modelo.insertarReserva(vista.fechaInicioReserva.getDate(),
                                vista.fechaFinReserva.getDate(),
                                String.valueOf(vista.comboClientes.getSelectedItem()),
                                String.valueOf(vista.comboHabitaciones.getSelectedItem()));
                        refrescarReservas();
                    }
                } catch (Exception ex) {
                    Util.showErrorAlert("Introduce correctamente las fechas");
                    vista.reservaTabla.clearSelection();
                }
                borrarCamposReservas();
            }
            break;
            case "modificarReserva": {
                try {
                    if (comprobarReservaVacio()) {
                        Util.showErrorAlert("Rellena todos los campos");
                        vista.reservaTabla.clearSelection();
                    } else {
                        modelo.modificarReserva(vista.fechaInicioReserva.getDate(), vista.fechaFinReserva.getDate(),String.valueOf(vista.comboClientes.getSelectedItem()),
                                String.valueOf(vista.comboHabitaciones.getSelectedItem()),
                                Integer.parseInt((String)vista.reservaTabla.getValueAt(vista.reservaTabla.getSelectedRow(), 0)));
                        refrescarReservas();
                    }
                } catch (Exception ex) {
                    Util.showErrorAlert("Introduce correctamente las fechas");
                    vista.reservaTabla.clearSelection();
                }
                borrarCamposReservas();
            }
            break;
            case "buscarPorNombre":
                try {

                    cargarFilas(modelo.buscarPorNombre(vista.txtBuscarNombre.getText()));
                } catch (SQLException ex) {
                    ex.printStackTrace();
                }

                break;
            case "eliminarReserva":
                modelo.borrarReserva(Integer.parseInt((String)vista.reservaTabla.getValueAt(vista.reservaTabla.getSelectedRow(), 0)));
                borrarCamposReservas();
                refrescarReservas();
                break;
            case "anadirHabitacion": {
                try {
                    if (comprobarHabitacionVacia()) {
                        Util.showErrorAlert("Rellena todos los campos");
                        vista.habitacionTabla.clearSelection();
                    } else if (modelo.habitacionNumeroYaExiste(Integer.parseInt(vista.txtNumeroHabitacion.getText()))) {
                        Util.showErrorAlert("Ese número ya existe.\nIntroduce un número diferente.");
                        vista.habitacionTabla.clearSelection();
                    } else {
                        modelo.insertarHabitacion(Integer.parseInt(vista.txtNumeroHabitacion.getText()),
                                String.valueOf(vista.comboTipoHabitacion.getSelectedItem()),
                                Float.parseFloat(vista.txtPrecioHabitacionPorNoche.getText()),
                                vista.txtVistas.getText(),vista.txtExtras.getText());
                        refrescarHabitacion();
                    }
                } catch (NumberFormatException nfe) {
                    Util.showErrorAlert("Introduce números en los campos que lo requieren");
                    vista.habitacionTabla.clearSelection();
                }
                borrarCamposHabitaciones();
            }
            break;
            case "modificarHabitacion": {
                try {
                    if (comprobarHabitacionVacia()) {
                        Util.showErrorAlert("Rellena todos los campos");
                        vista.habitacionTabla.clearSelection();
                    } else {
                        modelo.modificarHabitacion(Integer.parseInt(vista.txtNumeroHabitacion.getText()),
                                String.valueOf(vista.comboTipoHabitacion.getSelectedItem()),
                                Float.parseFloat(vista.txtPrecioHabitacionPorNoche.getText()),
                                vista.txtVistas.getText(), vista.txtExtras.getText(),
                                Integer.parseInt((String)vista.habitacionTabla.getValueAt(vista.habitacionTabla.getSelectedRow(), 0)));
                        refrescarHabitacion();
                    }
                } catch (NumberFormatException nfe) {
                    Util.showErrorAlert("Introduce números en los campos que lo requieren");
                    vista.habitacionTabla.clearSelection();
                }
                borrarCamposHabitaciones();
            }
            break;
            case "eliminarHabitacion":
                modelo.borrarHabitacion(Integer.parseInt((String)vista.habitacionTabla.getValueAt(vista.habitacionTabla.getSelectedRow(), 0)));
                borrarCamposHabitaciones();
                refrescarHabitacion();
                break;
        }
    }
    /**
     * Método que recibe un objeto ResultSet y nos carga cada elemento del ResultSet en un objeto de la clase Object
     * y nos lo añade a la clase addRow().
     * @param resultSet Objeto de la clase ResultSet
     * @throws SQLException Tipo de excepción.
     */

    private void cargarFilas(ResultSet resultSet) throws SQLException{

        if(resultSet!=null) {
            Object[] fila = new Object[9];
            vista.dtmBuscarClientes.setRowCount(0);

            while (resultSet.next()) {
                fila[0] = resultSet.getObject(1);
                fila[1] = resultSet.getObject(2);
                fila[2] = resultSet.getObject(3);
                fila[3] = resultSet.getObject(4);
                fila[4] = resultSet.getObject(5);
                fila[5] = resultSet.getObject(6);
                fila[6] = resultSet.getObject(7);
                fila[7] = resultSet.getObject(8);


                vista.dtmBuscarClientes.addRow(fila);
            }

        }

    }

    /**
     Método que nos podrá los nombres de las columnas en el TablaModel dtmBuscarClientes
     */
    private void iniciarTablaBuscar(){
        String[] headers={"Id","Nombre","Apellidos", "Fecha nacimiento", "Domicilio", "Dni", "Número móvil",
                "Tipo cliente"};
        vista.dtmBuscarClientes.setColumnIdentifiers(headers);
    }

    @Override
    public void windowOpened(WindowEvent e) {

    }

    @Override
    public void windowClosing(WindowEvent e) {
        System.exit(0);
    }

    @Override
    public void windowClosed(WindowEvent e) {

    }

    @Override
    public void windowIconified(WindowEvent e) {

    }

    @Override
    public void windowDeiconified(WindowEvent e) {

    }

    @Override
    public void windowActivated(WindowEvent e) {

    }

    @Override
    public void windowDeactivated(WindowEvent e) {

    }

    /**
     * Vacía los campos de la tabla de reservas
     */
    private void borrarCamposReservas() {
        vista.fechaInicioReserva.setText("");
        vista.fechaFinReserva.setText("");
        vista.comboClientes.setSelectedIndex(-1);
        vista.comboHabitaciones.setSelectedIndex(-1);
    }

    /**
     * Vacía los campos de la tabla de clientes
     */
    private void borrarCamposClientes() {
        vista.txtNombreCliente.setText("");
        vista.txtApellidosCliente.setText("");
        vista.fechaNacimientoCliente.setText("");
        vista.txtDomicilioCliente.setText("");
        vista.txtDNICliente.setText("");
        vista.txtNumeroMovilCliente.setText("");
        vista.comboTipoCliente.setSelectedIndex(-1);
    }

    /**
     * Vacía los campos de la tabla de habitaciones
     */
    private void borrarCamposHabitaciones() {
        vista.txtNumeroMovilCliente.setText("");
        vista.comboTipoHabitacion.setSelectedIndex(-1);
        vista.txtPrecioHabitacionPorNoche.setText("");
        vista.txtVistas.setText("");
        vista.txtExtras.setText("");

    }


    /**
     * Pones los títulos de la tabla.
     */
    private void iniciarTablaCantidadPorTipo(){
        String[] headers={"Tipo","Cantidad"};
        vista.dtmCantidadTipoHabitaciones.setColumnIdentifiers(headers);
    }

    /**
     *
     * @param resultSet
     * @throws SQLException
     */
    private void cargarFilas1(ResultSet resultSet) throws SQLException {
        Object[] fila=new Object[2];
        vista.dtmCantidadTipoHabitaciones.setRowCount(0);

        while (resultSet.next()){
            fila[0]=resultSet.getObject(1);
            fila[1]=resultSet.getObject(2);

            vista.dtmCantidadTipoHabitaciones.addRow(fila);
        }

    }

    /**
     * Método que recibe un objeto ResultSet y nos carga cada elemento del ResultSet en un objeto de la clase Object
     * y nos lo añade a la clase addRow().
     * @param resultSet Objeto de la clase ResultSet
     * @throws SQLException Tipo de excepción.
     */

    private void cargarFilas2(ResultSet resultSet) throws SQLException {
        Object[] fila=new Object[5];
        vista.dtmReservas.setRowCount(0);

        while (resultSet.next()){
            fila[0]=resultSet.getObject(1);
            fila[1]=resultSet.getObject(2);
            fila[2]=resultSet.getObject(3);
            fila[3]=resultSet.getObject(4);
            fila[4]=resultSet.getObject(5);

            vista.dtmReservas.addRow(fila);
        }

    }

    /**
     * Actualiza las clientes que se ven en la lista y los comboboxes
     */
    private void refrescarClientes() {
        try {
            vista.clientesTabla.setModel(construirTableModelClientes(modelo.consultarCliente()));
            vista.comboClientes.removeAllItems();
            for(int i = 0; i < vista.dtmClientes.getRowCount(); i++) {
                vista.comboClientes.addItem(vista.dtmClientes.getValueAt(i, 0)+" - "+
                        vista.dtmClientes.getValueAt(i, 1));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    private  DefaultTableModel construirTableModelClientes(ResultSet rs)
            throws SQLException {

        ResultSetMetaData metaData = rs.getMetaData();

        // names of columns
        Vector<String> columnNames = new Vector<>();
        int columnCount = metaData.getColumnCount();
        for (int column = 1; column <= columnCount; column++) {
            columnNames.add(metaData.getColumnName(column));
        }

        // data of the table
        Vector<Vector<Object>> data = new Vector<>();
        setDataVector(rs, columnCount, data);

        vista.dtmClientes.setDataVector(data, columnNames);





        return vista.dtmClientes;

    }


    /**
     * Actualiza las reservas que se ven en la lista
     */
    private void refrescarReservas() {
        try {
            vista.reservaTabla.setModel(construirTableModeloAutores(modelo.consultarReserva()));

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    private DefaultTableModel construirTableModeloAutores(ResultSet rs)
            throws SQLException {

        ResultSetMetaData metaData = rs.getMetaData();

        // names of columns
        Vector<String> columnNames = new Vector<>();
        int columnCount = metaData.getColumnCount();
        for (int column = 1; column <= columnCount; column++) {
            columnNames.add(metaData.getColumnName(column));
        }

        // data of the table
        Vector<Vector<Object>> data = new Vector<>();
        setDataVector(rs, columnCount, data);

        vista.dtmReservas.setDataVector(data, columnNames);

        return vista.dtmReservas;

    }

    /**
     * Actualiza las habitaciones que se ven en la lista y los comboboxes
     */
    private void refrescarHabitacion() {
        try {
            vista.habitacionTabla.setModel(construirTableModelHabitaciones(modelo.consultarHabitacion()));
            vista.comboHabitaciones.removeAllItems();
            for(int i = 0; i < vista.dtmHabitaciones.getRowCount(); i++) {
                vista.comboHabitaciones.addItem(vista.dtmHabitaciones.getValueAt(i, 0)+" - "+
                        vista.dtmHabitaciones.getValueAt(i, 1));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    private DefaultTableModel construirTableModelHabitaciones(ResultSet rs)
            throws SQLException {

        ResultSetMetaData metaData = rs.getMetaData();

        // names of columns
        Vector<String> columnNames = new Vector<>();
        int columnCount = metaData.getColumnCount();
        for (int column = 1; column <= columnCount; column++) {
            columnNames.add(metaData.getColumnName(column));
        }

        // data of the table
        Vector<Vector<Object>> data = new Vector<>();
        setDataVector(rs, columnCount, data);

        vista.dtmHabitaciones.setDataVector(data, columnNames);

        return vista.dtmHabitaciones;

    }

    @Override
    public void itemStateChanged(ItemEvent e) {

    }

    /**
     * Configura las opciones del dialog de opciones en función
     * de las opciones que el usuario guardo en la última ejecución del programa.
     */

    private void setOptions() {
        vista.optionDialog.tfIP.setText(modelo.getIP());
        vista.optionDialog.tfUser.setText(modelo.getUser());
        vista.optionDialog.pfPass.setText(modelo.getPassword());
        vista.optionDialog.pfAdmin.setText(modelo.getAdminPassword());
    }

    private void setDataVector(ResultSet rs, int columnCount, Vector<Vector<Object>> data) throws SQLException {
        while (rs.next()) {
            Vector<Object> vector = new Vector<>();
            for (int columnIndex = 1; columnIndex <= columnCount; columnIndex++) {
                vector.add(rs.getObject(columnIndex));
            }
            data.add(vector);
        }
    }


    /**
     * Comprueba que los campos necesarios para añadir un cliente estén vacíos
     *
     * @return True si al menos uno de los campos está vacío
     */
    private boolean comprobarClienteVacio() {
        return vista.txtNombreCliente.getText().isEmpty() ||
                vista.txtApellidosCliente.getText().isEmpty() ||
                vista.fechaNacimientoCliente.getText().isEmpty() ||
                vista.txtDomicilioCliente.getText().isEmpty() ||
                vista.txtDNICliente.getText().isEmpty() ||
                vista.txtNumeroMovilCliente.getText().isEmpty() ||
                vista.comboTipoCliente.getSelectedIndex() == -1;
    }

    /**
     * Comprueba que los campos necesarios para añadir una reserva estén vacíos
     *
     * @return True si al menos uno de los campos está vacío
     */
    private boolean comprobarReservaVacio() {
        return vista.fechaInicioReserva.getText().isEmpty() ||
                vista.fechaFinReserva.getText().isEmpty() ||
                vista.comboClientes.getSelectedIndex() == -1 ||
                vista.comboHabitaciones.getSelectedIndex() == -1 ;
    }

    /**
     * Comprueba que los campos necesarios para añadir una habitación estén vacíos
     *
     * @return True si al menos uno de los campos está vacío
     */
    private boolean comprobarHabitacionVacia() {
        return vista.txtNumeroHabitacion.getText().isEmpty() ||
                vista.comboTipoHabitacion.getSelectedIndex() == -1 ||
                vista.txtPrecioHabitacionPorNoche.getText().isEmpty() ||
                vista.txtVistas.getText().isEmpty() ||
                vista.txtExtras.getText().isEmpty();
    }
    private void addItemListeners(Controlador controlador) {
    }
}
