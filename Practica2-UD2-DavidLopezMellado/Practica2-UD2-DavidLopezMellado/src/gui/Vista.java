/**
 * @author David López Mellado
 * @since 1.8
 * @version 1.0
 */
package gui;

import base.enums.TiposClientes;
import base.enums.TiposHabitaciones;
import com.github.lgooddatepicker.components.DatePicker;
import com.github.lgooddatepicker.components.DateTimePicker;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.awt.*;

public class Vista extends JFrame{
    private JPanel panel1;
    private JTabbedPane tabbedPane1;


    private final static String TITULOFRAME="BASE DATOS HOTEL";

    /*CLIENTES*/
     JTextField txtNombreCliente;
     JTextField txtApellidosCliente;
     JTextField txtDomicilioCliente;
     JTextField txtDNICliente;
     JButton btnClientesAnadir;
     JButton btnClientesModificar;
     JButton btnClientesEliminar;
     JTable clientesTabla;
    JTextField txtNumeroMovilCliente;
    DatePicker fechaNacimientoCliente;
    JComboBox comboTipoCliente;
    JButton btnBuscarNombre;
    JTextField txtBuscarNombre;
    JTable clientesBuscarTabla;
     /*RESERVAS*/
     JButton btnReservasAnadir;
     JButton btnReservasEliminar;
    JComboBox comboClientes;
    JComboBox comboHabitaciones;
    JButton btnReservasModificar;
    JButton btnReservasMostrarMes;
    JTable reservaTabla;
    DatePicker fechaInicioReserva;
    DatePicker fechaFinReserva;
    JButton btnMostrarTodasReservas;
     /*HABITACIONES*/
    JButton btnHabitacionesAnadir;
     JButton btnHabitacionesModificar;
     JButton btnHabitacionesEliminar;
     JTextField txtNumeroHabitacion;
     JTextField txtPrecioHabitacionPorNoche;
     JTable habitacionTabla;
     JComboBox comboTipoHabitacion;
     JButton btnHabitacionesVisualizar;
     JButton btnHabitacionCantidadPorTipo;
     JComboBox comboTipoHabitacionBuscar;
     JTable cantidadTipoTabla;
     JTextField txtVistas;
     JTextField txtExtras;
    JComboBox comboTipoVistas;









 /*SEARCH*/
 JLabel etiquetaEstado;
 /*DEFAULT TABLE MODELS*/
 DefaultTableModel dtmClientes;
 DefaultTableModel dtmReservas;
 DefaultTableModel dtmHabitaciones;
 DefaultTableModel dtmBuscarClientes;
 DefaultTableModel dtmCantidadTipoHabitaciones;
 /*MENUBAR*/
 JMenuItem itemOpciones;
 JMenuItem itemDesconectar;
 JMenuItem itemSalir;
 JMenuItem itemCrearBaseDatos;
 JMenuItem itemCrearTablaClientes;
 JMenuItem itemCrearTablaReservas;
 JMenuItem itemCrearTablaHabitaciones;
 JMenuItem itemConectar;
 /*OPTION DIALOG*/
 OptionDialog optionDialog;
 /*SAVECHANGESDIALOG*/
 JDialog adminPasswordDialog;
 JButton btnValidate;
 JPasswordField adminPassword;

         /**
          * Constructor de la clase.
          * Setea el título de la ventana y llama al metodo que la inicia
          */
         public Vista() {
          super(TITULOFRAME);
          initFrame();
         }




  /**
   * Configura la ventana y la hace visible
   */
  private void initFrame() {
   this.setContentPane(panel1);
   this.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
   this.pack();
   this.setVisible(true);
   this.setSize(new Dimension(this.getWidth()+200, this.getHeight()+100));
   this.setLocationRelativeTo(null);
   this.optionDialog = new OptionDialog(this);
   setMenu();
   setAdminDialog();
   setEnumComboBox();
   setTableModels();
  }

   /**
    * Inicializa los DefaultTableModel, los setea en sus respectivas tablas
    */
   private void setTableModels() {

     this.dtmClientes = new DefaultTableModel();
     this.clientesTabla.setModel(dtmClientes);

     this.dtmReservas = new DefaultTableModel();
     this.reservaTabla.setModel(dtmReservas);

     this.dtmHabitaciones = new DefaultTableModel();
     this.habitacionTabla.setModel(dtmHabitaciones);

     this.dtmBuscarClientes= new DefaultTableModel();
     this.clientesBuscarTabla.setModel(dtmBuscarClientes);

     this.dtmCantidadTipoHabitaciones = new DefaultTableModel();
     this.cantidadTipoTabla.setModel(dtmCantidadTipoHabitaciones);

   }


    /**
     * Modifica los JCombobox añadiéndole los valores de las clases enum correspondientes.
     */


    private void setEnumComboBox() {
        for(TiposClientes constant : TiposClientes.values()) { comboTipoCliente.addItem(constant.getValor()); }
        comboTipoCliente.setSelectedIndex(-1);

        for(TiposHabitaciones constant : TiposHabitaciones.values()) { comboTipoHabitacion.addItem(constant.getValor()); }
        comboTipoHabitacion.setSelectedIndex(-1);
    }

      /**
       * Setea una barra de menus
       */
      private void setMenu(){
       JMenuBar mbBar = new JMenuBar();
       JMenu menu = new JMenu("Archivo");
       itemOpciones = new JMenuItem("Opciones");
       itemOpciones.setActionCommand("Opciones");
       itemConectar = new JMenuItem("Conectar");
       itemConectar.setActionCommand("Conectar");
       itemCrearBaseDatos = new JMenuItem("Crear base de datos");
       itemCrearBaseDatos.setActionCommand("CrearBaseDatos");
       itemCrearTablaClientes = new JMenuItem("Crear tabla clientes");
       itemCrearTablaClientes.setActionCommand("CrearTablaClientes");
       itemCrearTablaReservas = new JMenuItem("Crear tabla reservas");
       itemCrearTablaReservas.setActionCommand("CrearTablaReservas");
       itemCrearTablaHabitaciones = new JMenuItem("Crear tabla habitaciones");
       itemCrearTablaHabitaciones.setActionCommand("CrearTablaHabitaciones");
       itemDesconectar = new JMenuItem("Desconectar");
       itemDesconectar.setActionCommand("Desconectar");
       itemSalir = new JMenuItem("Salir");
       itemSalir.setActionCommand("Salir");
       menu.add(itemOpciones);
       menu.add(itemConectar);
       menu.add(itemCrearBaseDatos);
       menu.add(itemCrearTablaClientes);
       menu.add(itemCrearTablaReservas);
       menu.add(itemCrearTablaHabitaciones);
       menu.add(itemDesconectar);
       menu.add(itemSalir);
       mbBar.add(menu);
       mbBar.add(Box.createHorizontalGlue());
       this.setJMenuBar(mbBar);
      }


       /**
        * Setea un JDialog para introducir la contreña de administrador y poder configurar la base de datos
        */
       private void setAdminDialog() {
        btnValidate = new JButton("Validar");
        btnValidate.setActionCommand("abrirOpciones");
        adminPassword = new JPasswordField();
        adminPassword.setPreferredSize(new Dimension(100, 26));
        Object[] options = new Object[] {adminPassword, btnValidate};
        JOptionPane jop = new JOptionPane("Introduce la contraseña"
                , JOptionPane.WARNING_MESSAGE, JOptionPane.YES_NO_OPTION, null, options);

        adminPasswordDialog = new JDialog(this, "Opciones", true);
        adminPasswordDialog.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
        adminPasswordDialog.setContentPane(jop);
        adminPasswordDialog.pack();
        adminPasswordDialog.setLocationRelativeTo(this);

       }






}
